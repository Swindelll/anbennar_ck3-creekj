﻿#Shows tooltip with racial trait chances for children
child_race_chance_father_tooltip_effect = {
	show_as_tooltip = {
		#Elf-human-orc group
		if = {
			limit = {
				OR = {
					AND = {
						$MOTHER$ = {
							has_trait = elf
						}
						$FATHER$ = {
							OR = {
								has_trait = half_elf
								has_trait = human
								has_trait = half_orc
							}
						}
					}
					AND = {
						$FATHER$ = {
							has_trait = elf
						}
						$MOTHER$ = {
							OR = {
								has_trait = half_elf
								has_trait = human
								has_trait = half_orc
							}
						}
					}
				}
			}
			custom_tooltip = child_race_tooltip
			add_trait_force_tooltip = half_elf
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						$MOTHER$ = {
							has_trait = orc
						}
						$FATHER$ = {
							OR = {
								has_trait = elf
								has_trait = half_elf
								has_trait = human
								has_trait = half_orc
							}
						}
					}
					AND = {
						$FATHER$ = {
							has_trait = orc
						}
						$MOTHER$ = {
							OR = {
								has_trait = elf
								has_trait = half_elf
								has_trait = human
								has_trait = half_orc
							}
						}
					}
				}
			}
			custom_tooltip = child_race_tooltip
			add_trait_force_tooltip = half_orc
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						$MOTHER$ = {
							has_trait = half_elf
						}
						$FATHER$ = {
							has_trait = half_orc
						}
					}
					AND = {
						$FATHER$ = {
							has_trait = half_elf
						}
						$MOTHER$ = {
							has_trait = half_orc
						}
					}
				}
			}
			random_list = {
				75 = {
					custom_tooltip = child_race_tooltip
					add_trait_force_tooltip = half_orc
				}
				25 = {
					custom_tooltip = child_race_tooltip
					add_trait_force_tooltip = half_elf
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						$MOTHER$ = {
							has_trait = half_elf
						}
						$FATHER$ = {
							has_trait = human
						}
					}
					AND = {
						$FATHER$ = {
							has_trait = half_elf
						}
						$MOTHER$ = {
							has_trait = human
						}
					}
				}
			}
			random_list = {
				75 = {
					custom_tooltip = child_race_tooltip
					add_trait_force_tooltip = human
				}
				25 = {
					custom_tooltip = child_race_tooltip
					add_trait_force_tooltip = half_elf
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						$MOTHER$ = {
							has_trait = half_orc
						}
						$FATHER$ = {
							has_trait = human
						}
					}
					AND = {
						$FATHER$ = {
							has_trait = half_orc
						}
						$MOTHER$ = {
							has_trait = human
						}
					}
				}
			}
			random_list = {
				25 = {
					custom_tooltip = child_race_tooltip
					add_trait_force_tooltip = human
				}
				75 = {
					custom_tooltip = child_race_tooltip
					add_trait_force_tooltip = half_orc
				}
			}
		}
		#Mules
		else_if = {
			limit = {
				OR = {
					AND = {
						$MOTHER$ = {
							has_trait = goblin
						}
						$FATHER$ = {
							OR = {
								has_trait = elf
								has_trait = half_elf
								has_trait = human
								has_trait = orc
								has_trait = half_orc
							}
						}
					}
					AND = {
						$FATHER$ = {
							has_trait = goblin
						}
						$MOTHER$ = {
							OR = {
								has_trait = elf
								has_trait = half_elf
								has_trait = human
								has_trait = orc
								has_trait = half_orc
							}
						}
					}
				}
			}
			custom_tooltip = child_race_tooltip
			add_trait_force_tooltip = half_goblin
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						$MOTHER$ = {
							has_trait = hobgoblin
						}
						$FATHER$ = {
							OR = {
								has_trait = elf
								has_trait = half_elf
								has_trait = human
								has_trait = orc
								has_trait = half_orc
							}
						}
					}
					AND = {
						$FATHER$ = {
							has_trait = hobgoblin
						}
						$MOTHER$ = {
							OR = {
								has_trait = elf
								has_trait = half_elf
								has_trait = human
								has_trait = orc
								has_trait = half_orc
							}
						}
					}
				}
			}
			custom_tooltip = child_race_tooltip
			add_trait_force_tooltip = half_hobgoblin
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						$MOTHER$ = {
							has_trait = halfling
						}
						$FATHER$ = {
							has_trait = gnome
						}
					}
					AND = {
						$FATHER$ = {
							has_trait = halfling
						}
						$MOTHER$ = {
							has_trait = gnome
						}
					}
				}
			}
			custom_tooltip = child_race_tooltip
			add_trait_force_tooltip = gnomling
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						$MOTHER$ = {
							has_trait = troll
						}
						$FATHER$ = {
							has_trait = ogre
						}
					}
					AND = {
						$FATHER$ = {
							has_trait = troll
						}
						$MOTHER$ = {
							has_trait = ogre
						}
					}
				}
			}
			custom_tooltip = child_race_tooltip
			add_trait_force_tooltip = trollkin
		}
		#Light-mules
		else_if = {
			limit = {
				OR = {
					AND = {
						$MOTHER$ = {
							has_trait = orc
						}
						$FATHER$ = {
							has_trait = ogre
						}
					}
					AND = {
						$FATHER$ = {
							has_trait = orc
						}
						$MOTHER$ = {
							has_trait = ogre
						}
					}
				}
			}
			custom_tooltip = child_race_tooltip
			add_trait_force_tooltip = ogrillon
		}
		else_if = {
			limit = {
				$MOTHER$ = {
					has_trait = ogrillon
				}
				$FATHER$ = {
					has_trait = orc
				}
			}
			random_list = {
				90 = {
					custom_tooltip = child_race_tooltip
					add_trait_force_tooltip = ogrillon
				}
				10 = {
					custom_tooltip = child_race_tooltip
					add_trait_force_tooltip = orc
				}
			}
		}
		else_if = {
			limit = {
				$MOTHER$ = {
					has_trait = ogrillon
				}
				$FATHER$ = {
					has_trait = ogre
				}
			}
			random_list = {
				90 = {
					custom_tooltip = child_race_tooltip
					add_trait_force_tooltip = ogrillon
				}
				10 = {
					custom_tooltip = child_race_tooltip
					add_trait_force_tooltip = ogre
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						$MOTHER$ = {
							has_trait = goblin
						}
						$FATHER$ = {
							has_trait = hobgoblin
						}
					}
					AND = {
						$FATHER$ = {
							has_trait = goblin
						}
						$MOTHER$ = {
							has_trait = hobgoblin
						}
					}
				}
			}
			custom_tooltip = child_race_tooltip
			add_trait_force_tooltip = lesser_hobgoblin
		}
		else_if = {
			limit = {
				$MOTHER$ = {
					has_trait = lesser_hobgoblin
				}
				$FATHER$ = {
					has_trait = goblin
				}
			}
			random_list = {
				90 = {
					custom_tooltip = child_race_tooltip
					add_trait_force_tooltip = lesser_hobgoblin
				}
				10 = {
					custom_tooltip = child_race_tooltip
					add_trait_force_tooltip = goblin
				}
			}
		}
		else_if = {
			limit = {
				$MOTHER$ = {
					has_trait = lesser_hobgoblin
				}
				$FATHER$ = {
					has_trait = hobgoblin
				}
			}
			random_list = {
				90 = {
					custom_tooltip = child_race_tooltip
					add_trait_force_tooltip = lesser_hobgoblin
				}
				10 = {
					custom_tooltip = child_race_tooltip
					add_trait_force_tooltip = hobgoblin
				}
			}
		}
		#Harpy
		else_if = {
			limit = {
				$MOTHER$ = {
					has_trait = harpy
				}
			}
			custom_tooltip = child_race_tooltip
			add_trait_force_tooltip = harpy
			custom_tooltip = male_harpies_stillborn
		}
	}
}
