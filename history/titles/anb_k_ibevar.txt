k_ibevar = {
	1020.10.31 = {
		holder = 75 #Ibenion ta'Lunetain
		government = feudal_government
	}
}

k_farranean = {
	1016.7.8 = {
		holder = 76 #Martin Farran
		government = feudal_government
	}
}

d_whistlevale = {
	1016.7.8 = {
		holder = 76 #Martin Farran
		government = feudal_government
	}
}

d_valefort = {
	1016.7.8 = {
		holder = 76 #Martin Farran
		government = feudal_government
	}
}

d_cursewood = {
	1020.11.8 = {
		holder = 88 #Serondar Tederfremh
		government = feudal_government
	}
}